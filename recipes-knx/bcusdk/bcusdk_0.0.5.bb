SUMMARY = "bcusdk - A free development environment for BCU1 and BCU2"
DESCRIPTION = "BCUSDK provides the interface and libraries to connect to an eib/knx bus"
HOMEPAGE = "https://www.auto.tuwien.ac.at/~mkoegler/index.php/bcusdk"

LICENSE = "LGPLv2.1"
LICENSE_${PN} = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"
LIC_FILES_CHKSUM = "file://COPYING;md5=94d55d512a9ba36caa9b7df079bae19f"

PR = "r0"

DEPENDS="libxml2 pthsem"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI = " \
		http://sourceforge.net/projects/bcusdk/files/bcusdk/bcusdk_${PV}.tar.gz \
		file://eibd.service \
 "

SRC_URI[md5sum] = "5f81bc4e6bb53564573d573e795a9a5f"
SRC_URI[sha256sum] = "014cdaafc36f223c584b39ae5d698dd037a8e15aba4e78a2e531b51ff1331304"

prefix="/usr/local"
exec_prefix="/usr/local"
oldincludedir="/usr/local/include"

EXTRA_OECONF = " \
		--verbose \
		--without-pth-test \
		--oldincludedir="${PKG_CONFIG_SYSROOT_DIR}/usr/local/include" \
		--with-pth=../../../pthsem/2.0.8-r0/build \
		--enable-onlyeibd \
		--enable-eibnetip \
		--enable-eibnetiptunnel \
		--enable-eibnetipserver \
		--enable-groupcache \
 "

do_install_append() {
    install -d ${D}${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/eibd.service ${D}${systemd_unitdir}/system
}

SYSTEMD_SERVICE_${PN}_append = " eibd.service"

inherit autotools gettext pkgconfig
