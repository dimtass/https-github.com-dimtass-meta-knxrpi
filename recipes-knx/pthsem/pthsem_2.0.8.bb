SUMMARY = "pthsem package needed from BCUSDK"
DESCRIPTION = "pthsem is a fork from pth, with support for semaphores added. It can be installed parallel to a normal pth and provides a compatibilty layer to compile GNU pth programs with pthsem.."
HOMEPAGE = "https://www.auto.tuwien.ac.at/~mkoegler/index.php/pth"

LICENSE = "LGPLv2.1"
LICENSE_${PN} = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"
LIC_FILES_CHKSUM = "file://COPYING;md5=4c603c471bc48b83d1bb6fd35e9b742a"

PR = "r0"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

prefix="/usr/local"
exec_prefix="/usr/local"

SRC_URI = " \
		http://sourceforge.net/projects/bcusdk/files/pthsem/pthsem_${PV}.tar.gz \
		file://0001-patch-pth-config.patch \
"

SRC_URI[md5sum] = "9144b26dcc27e67498d63dd5456f934c"
SRC_URI[sha256sum] = "4024cafdd5d4bce2b1778a6be5491222c3f6e7ef1e43971264c451c0012c5c01"

inherit autotools
